* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1

# INSTALL
php artisan migrate
php artisan db:seed --class=SourcesTableSeeder
php artisan create:user


## Tests
php vendor/bin/phpunit

## Start


## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
