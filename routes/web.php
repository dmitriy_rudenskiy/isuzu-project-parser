<?php

use Illuminate\Support\Facades\Route;

Route::get('/',  'Admin\LoginController@showLoginForm')->name('login');

Route::group(['middleware' => 'web', 'namespace' => 'Admin'], function() {
    Route::post('login',  'LoginController@login');
    Route::get('login',  'LoginController@showLoginForm')->name('login');
    Route::get('logout',  'LoginController@logout')->name('logout');
});

Route::group(['middleware' => 'auth', 'prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin_'], function () {

    // файлы
    Route::group(['prefix' => 'files', 'as' => 'files_'], function () {
        Route::get('/', 'FileController@index')->name('index');
        Route::get('add', 'FileController@add')->name('add');
        Route::get('view/{id}', 'FileController@view')->name('view');
        Route::get('upload', 'FileController@upload')->name('upload');
        Route::post('upload', 'FileController@upload')->name('upload');
        Route::get('confirm', 'FileController@confirm')->name('confirm');
    });

    // запросы
    Route::group(['prefix' => 'queries', 'as' => 'queries_'], function () {
        Route::get('/', 'QueryController@index')->name('index');
        Route::get('view/{id}', 'QueryController@view')->name('view');
        Route::get('all/{id}', 'QueryController@all')->name('all');
        Route::get('unique/{id}', 'QueryController@unique')->name('unique');
    });
});