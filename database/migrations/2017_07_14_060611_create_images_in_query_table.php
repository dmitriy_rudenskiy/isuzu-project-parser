<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesInQueryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images_in_query', function (Blueprint $table) {
            $table->integer('query_id')->unsigned();
            $table->integer('image_id')->unsigned();

            $table->foreign('query_id')->references('id')->on('queries');
            $table->foreign('image_id')->references('id')->on('images');

            $table->primary(['query_id', 'image_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images_in_query');
    }
}
