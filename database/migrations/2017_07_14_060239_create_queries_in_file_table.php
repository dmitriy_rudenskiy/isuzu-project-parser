<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueriesInFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queries_in_file', function (Blueprint $table) {
            $table->integer('file_id')->unsigned();
            $table->integer('query_id')->unsigned();
            $table->boolean('is_duplicate');

            $table->foreign('file_id')->references('id')->on('files');
            $table->foreign('query_id')->references('id')->on('queries');

            $table->primary(['file_id', 'query_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('queries_in_file');
    }
}
