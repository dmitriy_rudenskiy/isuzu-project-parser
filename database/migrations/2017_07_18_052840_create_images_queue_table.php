<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images_queue', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_check')->default(false);
            $table->boolean('is_ready')->default(false);
            $table->string('url');
            $table->string('image');
            $table->string('title');
            $table->string('thumbnail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images_queue');
    }
}
