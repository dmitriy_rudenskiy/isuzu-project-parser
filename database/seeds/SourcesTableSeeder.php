<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            [
                'id' => 1,
                'name' => 'lsuzu.ru'
            ],
            [
                'id' => 2,
                'name' => 'isuzy.ru'
            ],
            [
                'id' => 3,
                'name' => 'izusu.ru'
            ],
            [
                'id' => 5,
                'name' => 'autodilir.ru'
            ],
            [
                'id' => 7,
                'name' => 'avtodillers.ru'
            ],
            [
                'id' => 9,
                'name' => 'dillersauto.ru'
            ]
        ];

        foreach ($list as $value) {
            try {
                DB::table('sources')->insert($value);
            } catch(\Exception $e) {

            }
        }
    }
}
