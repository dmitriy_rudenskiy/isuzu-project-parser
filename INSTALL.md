- создаем таблицы
php artisan migrate

- создаем администратора
php artisan create:user

php artisan db:seed --class=SourcesTableSeeder

INSERT INTO `sources` (`id`, `name`) VALUES ('5', 'autodilir.ru');
INSERT INTO `sources` (`id`, `name`) VALUES ('7', 'avtodillers.ru');
INSERT INTO `sources` (`id`, `name`) VALUES ('9', 'dillersauto.ru');
INSERT INTO `sources` (`id`, `name`) VALUES ('2', 'isuzy.ru');
INSERT INTO `sources` (`id`, `name`) VALUES ('3', 'izusu.ru');
INSERT INTO `sources` (`id`, `name`) VALUES ('1', 'lsuzu.ru');