<?php
namespace App\Entities;

use App\Tools\ImagePath;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'vendor_id',
        'visible',
        'download',
        'hash',
        'title',
        'image_url',
        'source_url'
    ];

    public $timestamps = false;

    /**
     * @param string$hash
     * @return Image|null
     */
    public function get($hash)
    {
        return self::where('hash', $hash)->first();
    }

    /**
     * @param string $hash
     * @return bool
     */
    public function has($hash)
    {
        $image = self::where('hash', $hash)->first();

        return ($image !== null);
    }

    /**
     * @param array $data
     * @return Image
     */
    public function add(array $data)
    {
        return self::forceCreate($data);
    }

    public function href()
    {
        return  implode(
            DIRECTORY_SEPARATOR,
            [
                '/gallery',
                ImagePath::getFull(),
                ImagePath::get($this->hash)
            ]
        );
    }
}