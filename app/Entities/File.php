<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class File extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'source_id',
        'name',
        'all',
        'new'
    ];

    protected $table = 'files';

    public function queries()
    {
        return $this->belongsToMany(Query::class, 'queries_in_file', 'file_id', 'query_id');
    }

    public function source()
    {
        return $this->hasOne(Source::class, 'id', 'source_id');
    }
}


