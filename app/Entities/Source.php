<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $table = 'sources';

    public $timestamps = false;

    public function __toString()
    {
        return $this->name;
    }
}