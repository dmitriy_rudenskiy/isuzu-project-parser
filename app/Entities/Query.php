<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Query extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'hash',
        'confirmed',
        'source_id',
        'text'
    ];

    protected $table = 'queries';

    public function source()
    {
        return $this->hasOne(Source::class, 'id', 'source_id');
    }

    public function images()
    {
        return $this->belongsToMany(Image::class, 'images_in_query', 'query_id', 'image_id');
    }
}