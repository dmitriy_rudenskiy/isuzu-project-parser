<?php

namespace App\Tools;

use InvalidArgumentException;

class ImagePath
{
    const SIZE_FULL = 'full';
    const SIZE_THUMBNAIL = 'thumbnail';
    const EXTENSION = '.jpg';

    public static function get($hash)
    {
        if (!is_string($hash) || strlen($hash) != 32) {
            throw new InvalidArgumentException();
        }

        return DIRECTORY_SEPARATOR
            . substr($hash, 0, 2)
            . DIRECTORY_SEPARATOR
            . substr($hash, 2, 2)
            . DIRECTORY_SEPARATOR
            . substr($hash, 4, 28)
            . self::EXTENSION;

    }

    public static function getFull()
    {
        return self::SIZE_FULL;
    }

    public static function getThumbnail()
    {
        return self::SIZE_THUMBNAIL;
    }
}