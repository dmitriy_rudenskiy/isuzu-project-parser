<?php

namespace App\Tools\Google\Exception;

use InvalidArgumentException;

class BadQuery extends InvalidArgumentException
{

}