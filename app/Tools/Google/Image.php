<?php
namespace App\Tools\Google;

class Image
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $imageUrl;

    /**
     * @var string
     */
    private $thumbnailUrl;

    public function __construct($obj)
    {
        if (!is_object($obj)) {
            return;
        }

        if (!empty($obj->snippet)) {
            $this->title = trim(str_replace('...', '', $obj->snippet));
        }

        if (!empty($obj->image->contextLink)) {
            $this->url = $obj->image->contextLink;
        }

        if (!empty($obj->link)) {
            $this->imageUrl = $obj->link;
        }

        if (!empty($obj->image->thumbnailLink)) {
            $this->thumbnailUrl = $obj->image->thumbnailLink;
        }
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @return string
     */
    public function getThumbnailUrl()
    {
        return $this->thumbnailUrl;
    }

    public function toArray()
    {
        return [
            'title' => $this->title,
            'url' => $this->url,
            'image' => $this->imageUrl,
            'thumbnail' => $this->thumbnailUrl
        ];
    }
}