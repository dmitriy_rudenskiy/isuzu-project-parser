<?php
namespace App\Tools\Google;

use App\Tools\Google\Exception\BadQuery;
use App\Tools\Google\Exception\NotInitConfig;

class Client
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $key;

    public function __construct()
    {
        $this->url = env('GOOGLE_API_URL');
        $this->key = env('GOOGLE_API_KEY');
    }

    /**
     * Отправляем запрос
     *
     * @param string$query
     * @return Image[]
     */
    public function get($query)
    {
        if (empty($query) || !is_string($query)) {
            throw new BadQuery();
        }

        if (empty($this->url) || empty($this->key)) {
            throw new NotInitConfig();
        }

        $options = [
            'q'=> $query,
            'filter' => 0,
            'lr' => 'lang_ar',
            'safe' => 'high',
            'searchType' => 'image',
            'siteSearch' => 10,
            'siteSearchFilter' => 'e',
            'start' => 1,
            'key'=> $this->key
        ];

        $url = $this->url . "?" . http_build_query($options) . "&cx=008938697304764600683%3Age54h--hsgc";

        $content = $this->sendRequest($url);

        $json = json_decode($content);

        if (empty($json->items) || sizeof($json->items) < 1) {
            return null;
        }

        $result = array_map(function($obj){
            return new Image($obj);
        }, $json->items);

        return $result;
    }

    /**
     * Отправляем запрос
     *
     * @return bool|string
     */
    public function sendRequest($url)
    {
        $content = file_get_contents($url);

        return $content;
    }
}