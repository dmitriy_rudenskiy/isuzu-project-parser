<?php

namespace App\Console;

use App\Console\Commands\CheckDownloadImages;
use App\Console\Commands\CreateFirstUser;
use App\Console\Commands\DownloadImageGoogle;
use App\Console\Commands\ImportListImageFromFiles;
use App\Console\Commands\ImportQueryFromFile;
use App\Console\Commands\LoadDataFromGoogle;
use App\Console\Commands\ReturnToDownload;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CreateFirstUser::class,
        CheckDownloadImages::class,
        ImportListImageFromFiles::class,
        ImportQueryFromFile::class,
        LoadDataFromGoogle::class,
        DownloadImageGoogle::class,
        ReturnToDownload::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(LoadDataFromGoogle::class)->daily();
        $schedule->command(DownloadImageGoogle::class)->hourly();
        $schedule->command(CheckDownloadImages::class)->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        Artisan::command('inspire', function () {
            $this->comment(Inspiring::quote());
        })->describe('Display an inspiring quote');
    }
}
