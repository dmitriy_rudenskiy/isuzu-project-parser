<?php
namespace App\Console\Commands;

use App\Entities\Image;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Illuminate\Console\Command;

class DownloadImageGoogle extends Command implements DownloadStatusInterface
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:load:image:google';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        do {
            $image = Image::where('download', 0)->orderBy('id')->first();

            if ($image !== null) {
                $this->saveImage($image->image_url, $image->hash);

                $this->info('Load image number:' . $image->id);

                $image->download = self::MARKER_DOWNLOAD;
                $image->save();
            }

        } while ($image !== null);
    }

    /**
     * @param $url
     * @param $hash
     * @return null
     */
    protected function saveImage($url, $hash)
    {
        $path = DIRECTORY_SEPARATOR
            . substr($hash, 0, 2)
            . DIRECTORY_SEPARATOR
            . substr($hash, 2, 2);

        $imagine = new Imagine();

        $dir = public_path('gallery') . DIRECTORY_SEPARATOR . 'full' . $path;

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        $file = $dir . DIRECTORY_SEPARATOR . substr($hash, 4, 28) . '.jpg';

        // файл уже существует
        if (file_exists($file)) {
            $this->error('File exists: ' . $file);
            return null;
        }

        $content = @file_get_contents($url);

        // не удалось загрузить картинку
        if (empty($content)) {
            $this->error('Not load: ' . $url);
            return null;
        }

        $image = @imagecreatefromstring($content);

        // не удалось загрузить картинку
        if ($image === false) {
            $this->info('Not image file:' . $url);
            return null;
        }

        // сохраняем изображение
        $imagine->load($content)
            ->thumbnail(new Box(800, 600), ImageInterface::THUMBNAIL_INSET)
            ->save($file, ['jpeg_quality' => 95]);


        $this->info('File save to:' . $file);


        $dir = storage_path('image') . DIRECTORY_SEPARATOR . 'thumbnail' . $path;

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        $file = $dir . DIRECTORY_SEPARATOR . substr($hash, 4, 28) . '.jpg';

        // файл уже существует
        if (!file_exists($file)) {
            // сохраняем обложку
            $imagine->load($content)
                ->thumbnail(new Box(360, 270), ImageInterface::THUMBNAIL_INSET)
                ->save($file, ['jpeg_quality' => 85]);
        }
    }
}

