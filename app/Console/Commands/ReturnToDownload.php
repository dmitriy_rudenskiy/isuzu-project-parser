<?php
namespace App\Console\Commands;

use App\Entities\Image;
use App\Tools\ImagePath;
use Illuminate\Console\Command;

class ReturnToDownload extends Command implements DownloadStatusInterface
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'return:to:download';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lastId = 0;

        do {
            $image = Image::where('download', self::MARKER_DOWNLOAD)
                ->where('id', '>', $lastId)
                ->orderBy('id')
                ->first();

            if ($image !== null) {
                $lastId = (int)$image->id;

                $this->info('Check:' . $lastId);

                $filename = public_path('gallery') . ImagePath::get($image->hash);

                if (!file_exists($filename)) {
                    $image->download = self::MARKER_NOT_FIND;
                    $image->save();
                }
            }

        } while ($image !== null);
    }
}

