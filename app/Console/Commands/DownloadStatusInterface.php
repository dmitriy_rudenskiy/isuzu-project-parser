<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.07.17
 * Time: 15:37
 */

namespace App\Console\Commands;


interface DownloadStatusInterface
{
    const MARKER_NOT_FIND = 0;
    const MARKER_DOWNLOAD = 1;
    const MARKER_CLEAR = 2;
}