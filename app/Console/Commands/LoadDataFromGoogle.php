<?php
namespace App\Console\Commands;

use App\Entities\Query;
use App\Entities\Image as ImageEntities;
use App\Models\QueryImage;
use App\Tools\Google\Client;
use App\Tools\Google\Image;
use Illuminate\Console\Command;

class LoadDataFromGoogle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:load:from:google';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        do {
            $query = Query::where('confirmed', 0)->orderBy('id')->first();

            if ($query !== null) {
                $this->load($query);
            }

        } while ($query !== null);
    }

    protected function load(Query $query)
    {
        // загружаем данные из Google
        $service = new Client();
        $content = $service->get(mb_strtolower($query->text));

        // сохраняем данные
        if ($content !== null && sizeof($content) > 0) {
            foreach ($content as $value) {
                $this->saveImage($query, $value);
            }

            // ставим отметку о просмотре
            $query->confirmed = 1;
            $query->save();
        }
    }

    protected function saveImage(Query $query, Image $image)
    {
        if (mb_strlen($image->getImageUrl()) > 190 || mb_strlen($image->getUrl()) > 190) {
            return null;
        }

        $hash = md5($image->getImageUrl());
        $modelImage = new ImageEntities();

        if ($modelImage->has($hash)) {
            $image = $modelImage->get($hash);
        } else {
            // записываем в базу данных
            $data = [
                'vendor_id' => 1,
                'hash' => $hash,
                'title' => $image->getTitle(),
                'image_url' => $image->getImageUrl(),
                'source_url' => $image->getUrl()
            ];

            $image = $modelImage->add($data);
        }

        QueryImage::add($query->id, $image->id);
    }
}

