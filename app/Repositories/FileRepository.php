<?php

namespace App\Repositories;

use App\Entities\File;
use Prettus\Repository\Eloquent\BaseRepository;

class FileRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return File::class;
    }

    /**
     * @param string $name
     * @param int $sourceId
     * @return File
     */
    public function add($name, $sourceId)
    {
        $data = [
            'name' => $name,
            'source_id' => $sourceId
        ];

        return $this->create($data);
    }
}