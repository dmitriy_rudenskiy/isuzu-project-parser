<?php

namespace App\Repositories;

use App\Entities\Query;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface QueryRepository
 * @package namespace App\Repositories;
 */
class QueryRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Query::class;
    }

    public function add($text, $sourceId)
    {
        $data = [
            'text' => $text,
            'source_id' => $sourceId,
            'hash' => md5($text)
        ];

        return $this->create($data);
    }

    public function get($text)
    {
        return $this->findByField('hash', md5($text))->first();
    }
}
