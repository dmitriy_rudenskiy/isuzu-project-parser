<?php

namespace App\Repositories;

use App\Entities\Source;
use Prettus\Repository\Eloquent\BaseRepository;

class SourceRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Source::class;
    }
}
