<?php

namespace App\Repositories;

use App\Entities\Query;
use App\Entities\QueryInFiles;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface QueryRepository
 * @package namespace App\Repositories;
 */
class QueryInFileRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return QueryInFiles::class;
    }

    /**
     * @param $fileId
     * @param $queryId
     * @return QueryInFiles
     */
    public function add($fileId, $queryId, $isDuplicate)
    {
        $data = [
            'file_id' => $fileId,
            'query_id' => $queryId,
            'is_duplicate' => $isDuplicate
        ];

        return $this->create($data);
    }
}
