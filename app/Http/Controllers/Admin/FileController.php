<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\FileRepository;
use App\Repositories\QueryInFileRepository;
use App\Repositories\QueryRepository;
use App\Repositories\SourceRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use InvalidArgumentException;

class FileController extends Controller
{
    const COUNT_IN_PAGE = 15;

    public function index(FileRepository $repository)
    {
        $list = $repository->orderBy('id', 'desc')->paginate();

        return view(
            'admin.file.index',
            [
                'list' => $list
            ]
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(SourceRepository $repository)
    {
        $list = $repository->all();

        return view('admin.file.add', ['list' => $list]);
    }

    public function view(FileRepository $repository, $id)
    {
        $file = $repository->find($id);
        $list = $file->queries()->paginate(self::COUNT_IN_PAGE);

        return view(
            'admin.file.view',
            [
                'file' => $file,
                'list' => $list
            ]
        );
    }

    public function upload(Request $request, SourceRepository $sourceRepository, FileRepository $fileRepository)
    {
        $sourceId = (int)$request->get('source_id', 0);

        $source = $sourceRepository->find($sourceId);

        if ($source === null) {
            throw new \RuntimeException();
        }

        /* @var \Illuminate\Http\UploadedFile $upload */
        $upload = $request->file('file');

        if (!is_readable($upload->getRealPath())) {
            throw new InvalidArgumentException();
        }

        $filename = sprintf("%s_%d", $upload->getClientOriginalName(), time());

        $fileEntity = $fileRepository->add($filename, $source->id);

        if (!empty($fileEntity->id)) {

            // читаем данные из файла
            $result = $this->readFile($upload->getRealPath(), $fileEntity->id, $source->id);

            // обновляем данные
            $fileEntity->update($result);
        }

        return redirect()->route('admin_files_view', ['id' => $fileEntity]);
    }

    public function confirm(QueryRepository $queryRepository)
    {
        $list = $queryRepository->all();

        return view('admin.file.confirm', ['list' => $list]);
    }

    /**
     * Читаем загруженный файл.
     *
     * @param string $path
     * @param int $fileId
     */
    protected function readFile($path, $fileId, $sourceId)
    {
        $result = [
            'all' => 0,
            'new' => 0
        ];

        $queryInFileRepository = new QueryInFileRepository(app());
        $queryRepository = new QueryRepository(app());

        $file = new \SplFileObject($path);
        $file->setCsvControl(',');
        $file->setFlags(\SplFileObject::READ_CSV);

        // один сайт
        foreach ($file as $row) {
            if (!empty($row[0])) {

                // увеличиваем счетчик
                $result['all']++;

                $text = $this->prepare($row[0]);

                $isDuplicate = true;

                // существует такой запрос
                $queryEntity = $queryRepository->get($text);

                if ($queryEntity === null) {
                    $isDuplicate = false;

                    // добавляем
                    $queryEntity = $queryRepository->add($text, $sourceId);
                    $result['new']++;
                }

                // добавляем связь файл <-> запрос
                $queryInFileRepository->add($fileId , $queryEntity->id, $isDuplicate);
            }
        }

        return $result;
    }

    /**
     * @param string $string
     * @param string $delimiter
     * @return string
     */
    protected function prepare($string, $delimiter = ' ')
    {
        $tmp = explode($delimiter, $string);

        $data = array_filter($tmp, 'strlen');

        $result = implode($delimiter, $data);

        return mb_strtoupper($result);
    }
}