<?php
namespace App\Http\Controllers\Admin;

use App\Entities\Query;
use App\Repositories\QueryRepository;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class QueryController extends Controller
{
    const COUNT_IN_PAGE = 15;

    public function index(QueryRepository $repository, Request $request)
    {
        $query = $request->get('query', '');

        if (!empty($query)) {
            $list = Query::where('text', 'like', '%' . $query . '%')
                ->orderBy('text')
                ->paginate(self::COUNT_IN_PAGE)
                ->appends(['text' => $query]);
        } else {
            $list = $repository->paginate(self::COUNT_IN_PAGE);
        }

        return view(
            'admin.query.index',
            [
                'list' => $list,
                'query' => $query
            ]
        );
    }

    public function view(QueryRepository $repository, $id)
    {
        $query = $repository->find($id);

        return view(
            'admin.query.view',
            [
                'query' => $query,
            ]
        );
    }

    public function all(QueryRepository $repository, $id)
    {
        $list = $repository->findWhere('file_id', $id)
                ->orderBy('text')
                ->paginate(self::COUNT_IN_PAGE);

        return view(
            'admin.query.view',
            [
                'list' => $list,
            ]
        );
    }

    public function unique(QueryRepository $repository, $id)
    {
        $list = $repository->findWhere('file_id', $id)
            ->orderBy('text')
            ->paginate(self::COUNT_IN_PAGE);

        return view(
            'admin.query.view',
            [
                'list' => $list,
            ]
        );
    }
}